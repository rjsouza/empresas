﻿using Enterprise.Model.Base;

namespace Enterprise.Model.User
{
    public class UserEnterpriseModel : EnterpriseBaseModel
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}