﻿using Enterprise.Model.Base;

namespace Enterprise.Model.Enterprise
{
    public class EnterpriseModel : EnterpriseBaseModel
    {
        public string Name { get; set; }

        public int Type { get; set; }
    }
}