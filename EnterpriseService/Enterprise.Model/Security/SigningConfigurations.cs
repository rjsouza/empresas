﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;

namespace Enterprise.Model.Security
{
    /// <summary>
    /// Gera as configurações para a chave de criptografia do token a ser enviado pelo chamador
    /// </summary>
    public class SigningConfigurations
    {
        /// <summary>
        /// Armazena a chave de criptografia utilizada para a criação do token
        /// </summary>
        public SecurityKey Key { get; }

        /// <summary>
        /// Contém a chave de criptografia e as configurações relacionadas ao algoritmo de segurança empregado na criação do token
        /// </summary>
        public SigningCredentials SigningCredentials { get; }

        public SigningConfigurations()
        {
            using (var provider = new RSACryptoServiceProvider(2048))
            {
                Key = new RsaSecurityKey(provider.ExportParameters(true));
            }

            SigningCredentials = new SigningCredentials(
                Key, SecurityAlgorithms.RsaSha256Signature);
        }
    }
}