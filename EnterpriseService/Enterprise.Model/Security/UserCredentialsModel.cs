﻿namespace Enterprise.Model.Security
{
    public class UserCredentialsModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}