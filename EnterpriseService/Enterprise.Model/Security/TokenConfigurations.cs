﻿namespace Enterprise.Model.Security
{
    /// <summary>
    ///Esta classe representa as configurações informadas no arquivo appsettings.json
    ///https://docs.microsoft.com/en-us/azure/active-directory/develop/id-tokens
    /// </summary>
    public class TokenConfigurations
    {
        /// <summary>
        /// Identificador do app
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// Emissor do token servidor
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// Validade do token (em segundos)
        /// </summary>
        public int Seconds { get; set; }
    }
}