﻿using Enterprise.Model.Base;
using System;

namespace Enterprise.Model.Security
{
    public class TokenEnterpriseModel : EnterpriseBaseModel
    {
        public bool Authenticated { get; set; }

        public DateTime Created { get; set; }

        public DateTime Expiration { get; set; }

        public string AccessToken { get; set; }

        public string Message { get; set; }

        public string Client { get; set; }
    }
}