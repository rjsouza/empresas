﻿using Enterprise.Model.Enterprise;
using Enterprise.Service.Interface.Base;
using System.Collections.Generic;

namespace Enterprise.Service.Interface.Enterprise
{
    public interface IEnterpriseService : IEnterpriseBaseService
    {
        EnterpriseModel Get(int id);

        IEnumerable<EnterpriseModel> GetEnterprises(int type, string name);
    }
}