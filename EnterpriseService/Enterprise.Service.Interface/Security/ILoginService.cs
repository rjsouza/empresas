﻿using Enterprise.Model.Security;
using Enterprise.Service.Interface.Base;

namespace Enterprise.Service.Interface.Security
{
    public interface ILoginService : IEnterpriseBaseService
    {
        TokenEnterpriseModel Authenticate(UserCredentialsModel userCredentials);
    }
}