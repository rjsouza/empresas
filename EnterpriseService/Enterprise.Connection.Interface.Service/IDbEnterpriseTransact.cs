﻿using System;

namespace Enterprise.Connection.Interface.Service
{
    public interface IDbEnterpriseTransact : IDisposable
    {
        void BeginTransaction();

        void Rollback();

        void Commit();

        void EndTransaction();
    }
}