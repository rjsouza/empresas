﻿using Enterprise.Repository.EntityModel.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enterprise.Repository.EntityModel
{
    [Table("tb_user")]
    public class UserEntity : EnterpriseBaseEntity
    {
        [Column("email")]
        public string Email { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}