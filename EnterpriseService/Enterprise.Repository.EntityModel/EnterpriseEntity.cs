﻿using Enterprise.Repository.EntityModel.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enterprise.Repository.EntityModel
{
    [Table("tb_enterprise")]
    public class EnterpriseEntity : EnterpriseBaseEntity
    {
        [Column("cnpj")]
        public string Cnpj { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("type_id")]
        public int Type { get; set; }
    }
}