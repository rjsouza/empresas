﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enterprise.Repository.EntityModel.Base
{
    public class EnterpriseBaseEntity
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }
    }
}