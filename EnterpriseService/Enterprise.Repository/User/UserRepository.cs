﻿using Enterprise.Connection.Interface;
using Enterprise.Model.Security;
using Enterprise.Model.User;
using Enterprise.Repository.Base;
using Enterprise.Repository.Interface.User;

namespace Enterprise.Repository.User
{
    public class UserRepository : EnterpriseBaseRepository, IUserRepository
    {
        public UserRepository(IDbEnterprise dbEnterprise)
            : base(dbEnterprise)
        {
        }

        public UserEnterpriseModel Find(UserCredentialsModel userCredentials)
        {
            var sql = @"select id as Id, name as Name, email as Email
                        from tb_user
                        where email = @email and password = @password";

            var userModel = this.SelectFirstOrDefault<UserEnterpriseModel>(sql, userCredentials);

            return userModel;
        }
    }
}