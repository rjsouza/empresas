﻿using Dapper;
using Enterprise.Connection.Interface;
using Enterprise.Repository.EntityModel.Base;
using Enterprise.Repository.Interface.Base;

namespace Enterprise.Repository.Base
{
    public abstract class EnterpriseBaseCrudRepository<TEntityModel> : EnterpriseBaseRepository, IEnterpriseBaseCrudRepository<TEntityModel>
        where TEntityModel : EnterpriseBaseEntity
    {
        public EnterpriseBaseCrudRepository(IDbEnterprise dbEnterprise)
            : base(dbEnterprise)
        {
        }

        public virtual void Delete(TEntityModel entityModel)
        {
            this.dbEnterprise.DB.Delete<TEntityModel>(entityModel, this.dbEnterprise.Transaction);
        }

        public TEntityModel Get(int id)
        {
            return this.dbEnterprise.DB.Get<TEntityModel>(id, this.dbEnterprise.Transaction);
        }

        public virtual void Insert(TEntityModel entityModel)
        {
            this.dbEnterprise.DB.Insert<TEntityModel>(entityModel, this.dbEnterprise.Transaction);
        }

        public virtual void Update(TEntityModel entityModel)
        {
            this.dbEnterprise.DB.Update<TEntityModel>(entityModel, this.dbEnterprise.Transaction);
        }
    }
}