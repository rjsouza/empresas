﻿using Dapper;
using Enterprise.Connection.Interface;
using Enterprise.Repository.Interface;
using System;
using System.Collections.Generic;

namespace Enterprise.Repository.Base
{
    public abstract class EnterpriseBaseRepository : IEnterpriseBaseRepository
    {
        protected readonly IDbEnterprise dbEnterprise;

        private bool disposedValue = false;

        public EnterpriseBaseRepository(IDbEnterprise dbEnterprise)
        {
            this.dbEnterprise = dbEnterprise;
        }

        protected virtual IEnumerable<TModel> Select<TModel>(string sql, object param)
        {
            return this.dbEnterprise.DB.Query<TModel>(sql, param);
        }

        protected virtual TModel SelectFirstOrDefault<TModel>(string sql, object param)
        {
            return this.dbEnterprise.DB.QueryFirstOrDefault<TModel>(sql, param);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}