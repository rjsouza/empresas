﻿using Enterprise.Connection.Interface;
using Enterprise.Model.Enterprise;
using Enterprise.Repository.Base;
using Enterprise.Repository.EntityModel;
using Enterprise.Repository.Interface.Enterprise;
using System.Collections.Generic;

namespace Enterprise.Repository.Enterprise
{
    public class EnterpriseRepository : EnterpriseBaseCrudRepository<EnterpriseEntity>, IEnterpriseRepository
    {
        public EnterpriseRepository(IDbEnterprise dbEnterprise)
            : base(dbEnterprise)
        {
        }

        public IEnumerable<EnterpriseModel> GetEnterprises(int type, string name)
        {
            var sql = @"select id as Id,
                               name as Name,
                               type_id as Type
                        from tb_enterprise
                        where type_id = @type or name = @name";
            var enterpriseList = this.Select<EnterpriseModel>(sql, new { type = type, name = name });

            return enterpriseList;
        }
    }
}