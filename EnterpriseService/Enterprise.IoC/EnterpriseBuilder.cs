﻿using Autofac;
using Enterprise.Connection;
using Enterprise.Connection.Interface;
using Enterprise.Connection.Interface.Service;
using Enterprise.IoC.Interfaces;
using Enterprise.Repository.Base;
using Enterprise.Service.Base;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Enterprise.IoC
{
    public static class EnterpriseBuilder
    {
        public static ILifetimeScope Build(IServiceCollection services,
            IAutofacAppBuilder autofacAppBuilder)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DbEnterprise>()
                .As<IDbEnterprise>()
                .As<IDbEnterpriseTransact>()
                .UsingConstructor(typeof(string))
                .WithParameters(new[] {
                    new NamedParameter("connectionString", autofacAppBuilder.Configuration.GetConnectionString("EnterpriseDb")),
                });

            builder.RegisterAssemblyTypes(Assembly.Load(typeof(EnterpriseBaseRepository).Assembly.GetName()))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            builder.RegisterAssemblyTypes(Assembly.Load(typeof(EnterpriseBaseService).Assembly.GetName()))
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            autofacAppBuilder.SetupContainerServices(builder, services);

            return builder.Build();
        }
    }
}