﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Enterprise.IoC.Interfaces
{
    public interface IAutofacAppBuilder
    {
        IConfigurationRoot Configuration { get; }

        void SetupContainerServices(ContainerBuilder builder, IServiceCollection services);
    }
}