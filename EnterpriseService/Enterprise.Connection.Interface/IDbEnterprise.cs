﻿using System;
using System.Data;

namespace Enterprise.Connection.Interface
{
    public interface IDbEnterprise : IDisposable
    {
        IDbConnection DB { get; }

        IDbTransaction Transaction { get; }
    }
}