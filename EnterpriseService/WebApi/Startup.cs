﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Enterprise.IoC;
using Enterprise.IoC.Interfaces;
using Enterprise.Model.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using WebApi.Filter;

namespace WebApi
{
    public class Startup : IAutofacAppBuilder
    {
        public IConfigurationRoot Configuration { get; private set; }

        public ILifetimeScope AutofacContainer { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddMvc(options =>
            {
                options.Filters.Add(new AuthChangeFilter());

                //Completa o cabeçalho de response a cada requisição, retornando ao cliente o token informado anteriormente
                options.Filters.Add(new AuthCompleteFilter());

                //Configuração para manter politica de autorização padrão para todas as controllers
                var policy = new AuthorizationPolicyBuilder()
                               .RequireAuthenticatedUser()
                               .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                               .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });

            //Configurar serviços para autenticação de token utilizando o modelo do JWT
            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                ///Aqui é feito uso das instancias dos serviços registrados anteriormente
                var signingConfigurations = this.AutofacContainer.Resolve<SigningConfigurations>();
                var tokenConfigurations = this.AutofacContainer.Resolve<TokenConfigurations>();

                // Aqui é passado os parametros de validação configurados nos objetos, de SigningConfigurations e TokenConfigurations
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = signingConfigurations.Key;
                paramsValidation.ValidAudience = tokenConfigurations.Audience;
                paramsValidation.ValidIssuer = tokenConfigurations.Issuer;

                // Valida a assinatura do token enviado
                paramsValidation.ValidateIssuerSigningKey = true;

                // Indica se deve verificar a validade do token
                paramsValidation.ValidateLifetime = true;

                // Tolerancia para validação de tempo do token
                paramsValidation.ClockSkew = TimeSpan.Zero;
            });

            //Ativa o uso do cabeçalho Bearer para acesso aos recursos deste token
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });

            //Registro dos objetos de container utilizando AutoFac
            AutofacContainer = EnterpriseBuilder.Build(services, this);

            return new AutofacServiceProvider(AutofacContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseMvc();
        }

        public void SetupContainerServices(ContainerBuilder builder, IServiceCollection services)
        {
            builder.Populate(services);

            builder.Register<SigningConfigurations>(c => new SigningConfigurations())
                   .SingleInstance()
                   .AsSelf();

            var tokenConfigurations = new TokenConfigurations();
            new ConfigureFromConfigurationOptions<TokenConfigurations>(
                Configuration.GetSection("TokenConfigurations"))
                    .Configure(tokenConfigurations);

            builder.Register<TokenConfigurations>(c => tokenConfigurations)
                   .SingleInstance()
                   .AsSelf();
        }
    }
}