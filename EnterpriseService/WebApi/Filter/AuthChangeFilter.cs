﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace WebApi.Filter
{
    public class AuthChangeFilter : IAuthorizationFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.Request.Headers.ContainsKey("access-token"))
                return;
            var clientTokenHeader = context.HttpContext.Request.Headers["access-token"];
            var clientToken = clientTokenHeader.FirstOrDefault();

            context.HttpContext.Request.Headers.Add("Authorization", $"Bearer {clientToken}");
        }
    }
}