﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;

namespace WebApi.Filter
{
    public class AuthCompleteFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (!context.HttpContext.Request.Headers.ContainsKey("access-token"))
                return;
            if (context.HttpContext.Response.Headers.ContainsKey("access-token"))
                return;

            var accessTokenHeader = context.HttpContext.Request.Headers["access-token"];
            var clientHeader = context.HttpContext.Request.Headers["client"];
            var uidHeader = context.HttpContext.Request.Headers["uid"];

            var accessToken = accessTokenHeader.FirstOrDefault();
            var client = clientHeader.FirstOrDefault();
            var uid = uidHeader.FirstOrDefault();

            context.HttpContext.Response.Headers.Add("access-token", accessToken);
            context.HttpContext.Response.Headers.Add("client", client);
            context.HttpContext.Response.Headers.Add("uid", uid);
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}