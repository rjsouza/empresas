﻿using Enterprise.Model.Enterprise;
using Enterprise.Service.Interface.Enterprise;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Route("api/v1/enterprises")]
    [ApiController]
    public class EnterpriseController : ControllerBase
    {
        private IEnterpriseService enterpriseService;

        public EnterpriseController(IEnterpriseService enterpriseService)
        {
            this.enterpriseService = enterpriseService;
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult<EnterpriseModel> Get(int id)
        {
            var enterprise = this.enterpriseService.Get(id);

            return Ok(enterprise);
        }

        [HttpGet]
        public ActionResult<IEnumerable<EnterpriseModel>> Get([FromQuery]int enterprise_types, [FromQuery]string name)
        {
            var enterpriseList = this.enterpriseService.GetEnterprises(enterprise_types, name);

            return Ok(enterpriseList);
        }
    }
}