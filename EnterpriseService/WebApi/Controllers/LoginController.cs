﻿using Enterprise.Model.Security;
using Enterprise.Service.Interface.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/v1/users")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService loginService;

        public LoginController(ILoginService loginService)
        {
            this.loginService = loginService;
        }

        [AllowAnonymous]
        [Route("auth/sign_in")]
        [HttpPost]
        public ActionResult<TokenEnterpriseModel> SignIn([FromBody]UserCredentialsModel userCretentials)
        {
            var authUser = this.loginService.Authenticate(userCretentials);

            this.HttpContext.Response.Headers.Add("access-token", authUser.AccessToken.ToString());
            this.HttpContext.Response.Headers.Add("client", authUser.Client);
            this.HttpContext.Response.Headers.Add("uid", authUser.Id.ToString());

            return authUser;
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult<string> Home()
        {
            return "Olá Mundo!";
        }
    }
}