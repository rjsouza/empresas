﻿using Enterprise.Model.Security;
using Enterprise.Model.User;

namespace Enterprise.Repository.Interface.User
{
    public interface IUserRepository : IEnterpriseBaseRepository
    {
        UserEnterpriseModel Find(UserCredentialsModel userCredentials);
    }
}