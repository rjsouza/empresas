﻿using Enterprise.Repository.EntityModel.Base;

namespace Enterprise.Repository.Interface.Base
{
    public interface IEnterpriseBaseCrudRepository<TEntityModel> : IEnterpriseBaseRepository
        where TEntityModel : EnterpriseBaseEntity
    {
        void Insert(TEntityModel entityModel);

        void Update(TEntityModel entityModel);

        void Delete(TEntityModel entityModel);

        TEntityModel Get(int id);
    }
}