﻿using Enterprise.Model.Enterprise;
using Enterprise.Repository.EntityModel;
using Enterprise.Repository.Interface.Base;
using System.Collections.Generic;

namespace Enterprise.Repository.Interface.Enterprise
{
    public interface IEnterpriseRepository : IEnterpriseBaseCrudRepository<EnterpriseEntity>
    {
        IEnumerable<EnterpriseModel> GetEnterprises(int type, string name);
    }
}