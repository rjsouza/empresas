﻿using Enterprise.Connection.Interface;
using Enterprise.Connection.Interface.Service;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Enterprise.Connection
{
    public class DbEnterprise : IDbEnterprise, IDbEnterpriseTransact
    {
        private readonly IDbConnection db;

        private bool disposedValue = false;

        private IDbTransaction dbTransaction;

        public IDbConnection DB => this.db;

        public IDbTransaction Transaction => this.dbTransaction;

        public DbEnterprise(string connectionString)
        {
            this.db = new SqlConnection(connectionString);
            this.db.Open();
        }

        public void BeginTransaction()
        {
            this.dbTransaction = this.db.BeginTransaction();
        }

        public void Rollback()
        {
            this.dbTransaction.Rollback();
        }

        public void Commit()
        {
            this.dbTransaction.Commit();
        }

        public void EndTransaction()
        {
            this.dbTransaction.Dispose();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (this.dbTransaction != null)
                    {
                        this.dbTransaction.Rollback();

                        this.dbTransaction.Dispose();
                        this.dbTransaction = null;
                    }

                    this.db.Close();
                    this.db.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}