﻿using Enterprise.Connection.Interface.Service;
using Enterprise.Model.Security;
using Enterprise.Repository.Interface.User;
using Enterprise.Service.Base;
using Enterprise.Service.Interface.Security;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;

namespace Enterprise.Service.Security
{
    public class LoginService : EnterpriseBaseService, ILoginService
    {
        private readonly SigningConfigurations signingConfigurations;

        private readonly TokenConfigurations tokenConfigurations;

        public IUserRepository UserRepository { get; set; }

        public LoginService(IDbEnterpriseTransact transaction,
                            SigningConfigurations signingConfigurations,
                            TokenConfigurations tokenConfigurations)
            : base(transaction)
        {
            this.signingConfigurations = signingConfigurations;
            this.tokenConfigurations = tokenConfigurations;
        }

        public TokenEnterpriseModel Authenticate(UserCredentialsModel userCredentials)
        {
            var user = this.UserRepository.Find(userCredentials);
            if (user == null)
                return new TokenEnterpriseModel() { Authenticated = false, Message = "Usuário não encontrado" };

            ClaimsIdentity identity = new ClaimsIdentity(
                   new GenericIdentity(user.Id.ToString(), "Login"),
                   new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString())
                   }
               );

            DateTime createdDate = DateTime.Now;
            DateTime expiredDate = createdDate + TimeSpan.FromSeconds(tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = tokenConfigurations.Issuer,
                Audience = tokenConfigurations.Audience,
                SigningCredentials = signingConfigurations.SigningCredentials,
                Subject = identity,
                NotBefore = createdDate,
                Expires = expiredDate
            });
            var token = handler.WriteToken(securityToken);

            return new TokenEnterpriseModel()
            {
                Id = user.Id,
                AccessToken = token,
                Authenticated = true,
                Client = tokenConfigurations.Audience,
                Created = createdDate,
                Expiration = expiredDate,
                Message = "ok"
            };
        }
    }
}