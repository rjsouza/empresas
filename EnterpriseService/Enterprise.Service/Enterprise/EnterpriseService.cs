﻿using Enterprise.Connection.Interface.Service;
using Enterprise.Model.Enterprise;
using Enterprise.Repository.Interface.Enterprise;
using Enterprise.Service.Base;
using Enterprise.Service.Interface.Enterprise;
using System.Collections.Generic;

namespace Enterprise.Service.Enterprise
{
    public class EnterpriseService : EnterpriseBaseService, IEnterpriseService
    {
        public IEnterpriseRepository EnterpriseRepository { get; set; }

        public EnterpriseService(IDbEnterpriseTransact transaction)
            : base(transaction)
        {
        }

        public EnterpriseModel Get(int id)
        {
            var enterpriseEntity = this.EnterpriseRepository.Get(id);
            var enterpriseModel = new EnterpriseModel();
            enterpriseModel.Id = enterpriseEntity.Id;
            enterpriseModel.Name = enterpriseEntity.Name;
            enterpriseModel.Type = enterpriseEntity.Type;

            return enterpriseModel;
        }

        public IEnumerable<EnterpriseModel> GetEnterprises(int type, string name)
        {
            var enterpriseList = this.EnterpriseRepository.GetEnterprises(type, name);

            return enterpriseList;
        }
    }
}