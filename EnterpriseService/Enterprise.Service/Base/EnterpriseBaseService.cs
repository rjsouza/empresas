﻿using Enterprise.Connection.Interface.Service;
using Enterprise.Service.Interface.Base;
using System;

namespace Enterprise.Service.Base
{
    public abstract class EnterpriseBaseService : IEnterpriseBaseService
    {
        private readonly IDbEnterpriseTransact transaction;

        private bool disposedValue = false;

        public EnterpriseBaseService(IDbEnterpriseTransact transaction)
        {
            this.transaction = transaction;
        }

        protected void Commit()
        {
            this.transaction.Commit();
        }

        protected void Rollback()
        {
            this.transaction.Rollback();
        }

        protected void BeginTransaction()
        {
            this.transaction.BeginTransaction();
        }

        protected void EndTransaction()
        {
            this.transaction.EndTransaction();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}