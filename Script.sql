use master;

drop database if exists enterprisedb;

create database enterprisedb;

use enterprisedb;

create table tb_user 
(
	id int primary key identity(1,1),
	name varchar(100) not null, 
	email varchar(50) not null unique ,
	password varchar(10) not null unique 
);

create table tb_enterprise_type(
	id int primary key identity(1,1),
	name varchar(50) not null,
	description varchar(100) not null
);

create table tb_enterprise(
	id int primary key identity(1,1),
	name varchar(150) not null,
	cnpj varchar(14) not null unique,
	type_id int not null,
	foreign key (type_id) references tb_enterprise_type(id)
);

-- Carregando dados de usu�rio para teste
insert into tb_user (name, email, password) values ('Ioasys', 'testeapple@ioasys.com.br', '12341234');

-- Carregando dados de empresas para teste
insert into tb_enterprise_type (name, description) values ('Fintech', 'Empresas financeiras de aplicativos');
insert into tb_enterprise_type (name, description) values ('Corretora', 'Corretoras financeiras');
insert into tb_enterprise_type (name, description) values ('Tecnlogia', 'Empresas fornecedoras de software ou produtos de tecnologia');

insert into tb_enterprise (name, cnpj, type_id) values ('Nubank', '18236120000158', 1);
insert into tb_enterprise (name, cnpj, type_id) values ('�rama', '13293225000125', 2);
insert into tb_enterprise (name, cnpj, type_id) values ('Apple Inc', '00623904000173', 3);
insert into tb_enterprise (name, cnpj, type_id) values ('Microsoft Informatica Ltda', '60316817000103', 3);